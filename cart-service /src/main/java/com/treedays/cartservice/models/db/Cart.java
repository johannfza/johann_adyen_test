package com.treedays.cartservice.models.db;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Cart {

    @Id
    private String id;
    private String userId;

    @OneToMany(mappedBy = "cart", fetch = FetchType.EAGER)
    private List<CartItem> items;

    public Cart() {
    }

    public Cart(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public Cart(String id, String userId, List<CartItem> items) {
        this.id = id;
        this.userId = userId;
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }


    // Public Methods
    public boolean containsProductId(String product_id) {
        boolean itemInList = false;
        for (int i = 0; i < this.getItems().size(); i++) {
            String p_id = this.items.get(i).getProductId();
            if (p_id.equals(product_id)) {
                itemInList = true;
                break;
            }
        }
        return itemInList;
    }
}
