INSERT INTO product (id, name, img_url, desc, price) VALUES
('p10001', 'Californication Vinyl by RHCP', '', 'The Red Hot Chilli Peppers of the quadruple-platinum smash Blood Sugar Sex Magic returned revitalised and reunited with Californication four years later', 39.50),
('p10002', 'Demon Days Vinyl by Gorillaz', '', 'The second studio album by the English virtual band, Features the single Feel Good Inc ', 54.15),
('p10003', 'Random Access Memories Vinyl by Daft Punk','', 'Random Access Memories is the new album from Daft Punk. Collaborators include Nile Rodgers, Chilly Gonzalez, Giorgio Moroder, Julian Casablancas, Pharrell Williams, Paul Williams, Todd Edwards, Panda Bear and DJ Falcon.', 29.90 ),
('p10004', 'Future Nostalgia Vinyl by Dua Lipa', '', 'Highly anticipated new album from Dua Lipa.', 39.95),
('p10005', 'Kind Of Blue Vinyl by Miles Davis', '', 'Miles said that he had wanted to draw closer to African and Gospel music as well as the blues, but admitted that he had failed in this intention.', 49.90),
('p10006', 'Blonde on Blonde Vinyl by Bob Dylan' ,'', 'Rock''s first great double-album and home to many of Dylan''s finest songs."', 35.50),
('p10007', 'Thriller Vinyl by Micheal Jackson', '', 'Two vinyl LP pressing of the 25th anniversary edition of Michael Jackson''s Thriller celebrates the groundbreaking album with eight bonus tracks, five of them previously unreleased.', 39.90 ),
('p10008', 'Dark Side of the Moon Vinyl by Pink Floyd', '', 'This album needs little introduction as I’m sure we’ve all heard the hits and the stories that it pairs perfectly with The Wizard of Oz.', 39.95);
