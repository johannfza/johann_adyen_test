package com.treedays.cartservice.models.db;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class CartItem {

    @Id
    private UUID id;
    private String productId;
    private int qty;

    @ManyToOne
    private Cart cart;

    public CartItem() {
    }

    public CartItem(UUID id, String productId, int qty) {
        this.id = id;
        this.productId = productId;
        this.qty = qty;
    }

    public CartItem(String productId, int qty, Cart cart) {
        this.id = UUID.randomUUID();
        this.productId = productId;
        this.qty = qty;
        this.cart = cart;
    }

    public CartItem(String productId, int qty) {
        this.id = UUID.randomUUID();
        this.productId = productId;
        this.qty = qty;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
