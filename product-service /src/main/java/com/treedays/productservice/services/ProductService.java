package com.treedays.productservice.services;

import com.treedays.productservice.models.Product;
import com.treedays.productservice.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public List<Product> getAll() {
        return (List<Product>) productRepository.findAll();
    }

    public Optional<Product> findById(String id){
        return productRepository.findById(id);
    }

    public List<Product> findProductByName(String s){
        return productRepository.findProductByNameContainingIgnoreCase(s);
    }

    public Product save(Product product){
        return productRepository.saveAndFlush(product);
    }

    public void delete(Product product){
        productRepository.delete(product);
    }

    public void deleteById(String id){
        productRepository.deleteById(id);
    }

}
