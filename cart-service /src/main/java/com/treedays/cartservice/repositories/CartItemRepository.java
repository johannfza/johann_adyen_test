package com.treedays.cartservice.repositories;

import com.treedays.cartservice.models.db.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, UUID> {

    void deleteCartItemsByCartId(String id);


}