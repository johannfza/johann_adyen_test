package com.treedays.cartservice.repositories;

import com.treedays.cartservice.models.db.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, String> {

    Cart findByUserId(String id);


}
