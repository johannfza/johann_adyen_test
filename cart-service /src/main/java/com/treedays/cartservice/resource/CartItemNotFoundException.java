package com.treedays.cartservice.resource;

import java.util.UUID;

public class CartItemNotFoundException extends RuntimeException {
    CartItemNotFoundException(UUID id) {
        super("Could not find CartItem with id: " + id);
    }

}
