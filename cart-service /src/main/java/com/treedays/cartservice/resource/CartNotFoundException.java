package com.treedays.cartservice.resource;


import java.util.UUID;

public class CartNotFoundException extends RuntimeException {
    CartNotFoundException(String id) {
        super("Could not find Cart with id: " + id);
    }

}
