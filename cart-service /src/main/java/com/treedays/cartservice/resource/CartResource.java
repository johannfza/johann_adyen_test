package com.treedays.cartservice.resource;

import com.treedays.cartservice.models.Product;
import com.treedays.cartservice.models.ResAmount;
import com.treedays.cartservice.models.ResCartItem;
import com.treedays.cartservice.models.db.Cart;
import com.treedays.cartservice.models.db.CartItem;
import com.treedays.cartservice.models.ResCart;
import com.treedays.cartservice.services.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/carts")
public class CartResource {

    private static final Logger log = LoggerFactory.getLogger(CartResource.class);

    private final CartService cartService;

    @Autowired
    private RestTemplate restTemplate;

    CartResource(CartService cartService) {
        this.cartService = cartService;
    }

    /* GET: Get all shopping carts */
    // curl http://localhost:8082/api/carts
    @GetMapping
    ResponseEntity<List<ResCart>> getAll() {
        List<Cart> carts = cartService.getAll();
        // Crate ResCart
        List<ResCart> resCarts = carts.stream().map(cart -> {
            List<CartItem> items = cart.getItems();
            // Get Products to fill ResCartItems
            List<ResCartItem> resCartItems = items.stream().map(item -> {
                Product product = restTemplate.getForObject("http://product-service/api/products/" + item.getProductId(), Product.class);
                return new ResCartItem(item.getId(), product, item.getQty());
            }).collect(Collectors.toList());
            // Fill ResCart Object
            return new ResCart(cart.getId(), cart.getUserId(), resCartItems);
        }).collect(Collectors.toList());
        return ResponseEntity.ok(resCarts);
    }

    /* GET: Get shopping Cart by customer id */
    // curl http://localhost:8082/api/carts/customer/u10001
    @GetMapping("/customer/{id}")
    ResponseEntity<ResCart> getByCustomerId(@PathVariable String id) {
        Cart cart = cartService.findByCustomerId(id);
        if (cart == null) {
            throw new CartNotFoundException(id);
        }
        return ResponseEntity.ok(getResCartResponseEntity(cart));
    }

    /* GET: Get shopping Cart by cart id */
    @GetMapping("{id}")
    ResponseEntity<ResCart> getByCartId(@PathVariable String id) {
        Optional<Cart> optCart = cartService.findById(id);
        if (optCart == null) {
            throw new CartNotFoundException(id);
        }
        Cart cart = optCart.get();
        return ResponseEntity.ok(getResCartResponseEntity(cart));
    }

    /* GET: Cart total by cart id*/
    @GetMapping("{id}/total")
    ResponseEntity<ResAmount> getTotal(@PathVariable String id){
        Optional<Cart> optCart = cartService.findById(id);
        if (optCart == null) {
            throw new CartNotFoundException(id);
        }
        Cart cart = optCart.get();
        ResCart resCart = getResCartResponseEntity(cart);
        ResAmount amount = new ResAmount(resCart.getMinorTotal());
        return ResponseEntity.ok(amount);
    }

    private ResCart getResCartResponseEntity(Cart cart) {
        List<ResCartItem> resCartItems = cart.getItems().stream().map(item -> {
            Product product = restTemplate.getForObject("http://product-service/api/products/" + item.getProductId(), Product.class);
            return new ResCartItem(item.getId(), product, item.getQty());
        }).collect(Collectors.toList());

        ResCart resCart = new ResCart(cart.getId(), cart.getUserId(), resCartItems);

        return resCart;
    }

    /* PUT: Update shopping Cart by cart id */
    @PutMapping("{id}")
    ResponseEntity<Cart> updateCart( @RequestBody Cart cart, @PathVariable String id) {
        Cart updatedCart;
        Optional<Cart> optionalCart = cartService.findById(cart.getId());
        if (optionalCart != null) {
            updatedCart = cartService.save(cart);
            log.info("Successfully updated cart with id: {}", id);
        } else {
            throw new CartNotFoundException(id);
        }
        return ResponseEntity.ok(updatedCart);
    }

    /* PUT: Clear cart */
    @PutMapping("/clear/{id}")
    ResponseEntity<Cart> clearCart(@PathVariable String id) {
        Cart updatedCart;
        Optional<Cart> optionalCart = cartService.findById(id);
        if (optionalCart != null) {
            updatedCart = cartService.clearCart(id);
        } else {
            throw new CartNotFoundException(id);
        }
        log.info("Successfully cleared cart with id: {}", id);
        return ResponseEntity.ok(updatedCart);
    }

    /* Endpoints for CartItems */

    /* POST: Add item to cart by cart id */
    @PostMapping("/items/add/{id}")
    ResponseEntity<Cart> addItemToCart(@RequestBody CartItem cartItem, @PathVariable String id) {
        Cart cart = cartService.addItemByCartId(cartItem, id);
        if (cart == null) {
            throw new CartNotFoundException(id);
        }
        log.info("Successfully add CartItem to cart id: {}", cart.getId());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        responseHeaders.set("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        responseHeaders.set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,observe");
        return ResponseEntity.ok().headers(responseHeaders).body(cart);
    }

//    /* POST: Add item to cart by cart id */
//    @PostMapping("/add/{id}/{productId}")
//    ResponseEntity<Cart> addSingleItemToCart(@RequestBody String productId, @PathVariable String id) {
//        Cart cart = cartService.addSingleItemToCart(productId, id);
//        if (cart == null) {
//            throw new CartNotFoundException(id);
//        }
//        log.info("Successfully add CartItem to cart id: {}", cart.getId());
//        return ResponseEntity.ok(cart);
//    }



//    /* POST: Add item to cart by customer id */
//    @PostMapping("/items/add/customer/{id}")
//    ResponseEntity<Cart> addItemToCartByCustomerId(@RequestBody CartItem cartItem, @PathVariable String id) {
//        Cart cart = cartService.addItemByCustomerId(cartItem, id);
//        if (cart == null) {
//            throw new CartNotFoundException(id);
//        }
//        log.info("Successfully add CartItem to cart id: {}", cart.getId());
//        return ResponseEntity.ok(cart);
//    }

    /* DELETE: Delete item from cart with cart id */
//    @DeleteMapping("/items/{item_id}/delete/{id}")
//    ResponseEntity<String> removeItemFromCart(@PathVariable String item_id, @PathVariable String id) {
//        Cart cart = cartService.removeCartItemFromCart(item_id, id);
//        if (cart == null) {
//            throw new CartNotFoundException(id);
//        }
//        log.info("Successfully deleted item {} in cart id: {}", item_id, id);
//        return ResponseEntity.ok(MessageFormat.format("Successfully deleted cartItem {0} cart with id: {1}", item_id, id));
//    }

    @DeleteMapping("/remove/item/{id}")
    ResponseEntity<String> removeCartItemFromCart(@PathVariable UUID id){
        CartItem cartItem = cartService.removeCartItem(id);
        if (cartItem == null){
            throw new CartItemNotFoundException(id);
        }
        log.info("Successfully deleted item {} in cart id: {}", id, cartItem.getId());
        return ResponseEntity.ok(MessageFormat.format("Successfully deleted cartItem {0} cart with id: {1}", id, cartItem.getId()));
    }

}

