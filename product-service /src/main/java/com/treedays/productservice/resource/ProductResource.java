package com.treedays.productservice.resource;

import com.treedays.productservice.models.Product;
import com.treedays.productservice.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/products")
public class ProductResource {

    private static final Logger log = LoggerFactory.getLogger(ProductResource.class);

    private final ProductService productService;

    ProductResource(ProductService productService) {
        this.productService = productService;
    }

    /* GET: Get all products */
    @GetMapping
    ResponseEntity<List<Product>> getAll() {
        log.debug("GET Products");
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        responseHeaders.set("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        responseHeaders.set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, observe");
        return ResponseEntity.ok().headers(responseHeaders).body(productService.getAll());
    }

    /* POST: Create new product */
    @PostMapping
    ResponseEntity<Void> newProduct(@RequestBody Product product) {
        Product newProduct = productService.save(product);
        if ( newProduct == null ) {
            return ResponseEntity.noContent().build();
        }
        log.info("Successfully created product with id: {}", newProduct.getId());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newProduct.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /* GET: Get product by id */
    @GetMapping("{id}")
    Product getById(@PathVariable String id) {
        return productService.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    /* PUT: Update product */
    @PutMapping("{id}")
    ResponseEntity<Product> updateProduct(@RequestBody Product product, @PathVariable String id) {
        Product updatedProduct;
        Optional<Product> optionalProduct = productService.findById(product.getId());
        if(optionalProduct != null){
            updatedProduct = productService.save(product);
            log.info("Successfully updated product with id: {}", id);
        } else {
            throw new ProductNotFoundException(id);
        }
        return ResponseEntity.ok(updatedProduct);
    }

    /* DELETE: Delete product */
    @DeleteMapping("{id}")
    ResponseEntity<String> deleteProduct(@PathVariable String id) {
        Optional<Product> optionalProduct = productService.findById(id);
        if(optionalProduct != null){
            productService.deleteById(id);
        } else {
            throw new ProductNotFoundException(id);
        }
        log.info("Successfully deleted product with id: {}", id);
        return ResponseEntity.ok(MessageFormat.format("Successfully deleted product with id: {0}", id ));
    }

}