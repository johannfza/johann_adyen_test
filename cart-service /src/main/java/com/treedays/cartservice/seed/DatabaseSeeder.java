package com.treedays.cartservice.seed;

import com.treedays.cartservice.models.db.Cart;
import com.treedays.cartservice.models.db.CartItem;
import com.treedays.cartservice.repositories.CartItemRepository;
import com.treedays.cartservice.repositories.CartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseSeeder {

    private static final Logger log = LoggerFactory.getLogger(DatabaseSeeder.class);
    private CartRepository cartRepository;
    private CartItemRepository cartItemRepository;

    @Autowired
    public DatabaseSeeder(CartRepository cartRepository, CartItemRepository cartItemRepository){
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event){
        cartItemRepository.deleteAll();
        cartRepository.deleteAll();
        seedCart();

    }

    private void seedCart(){
        log.info("Seeding Carts");
        Cart cart1 = new Cart("c10001", "u10001");
        Cart cart2 = new Cart("c10002", "u10002");
        cartRepository.saveAndFlush(cart1);
        cartRepository.saveAndFlush(cart2);
        List<CartItem> items = new ArrayList<>();
        items.add(new CartItem("p10001", 1, cart1));
        items.add(new CartItem("p10002", 1, cart1));
        items.add(new CartItem("p10003", 1, cart1));
        cartItemRepository.saveAll(items);

    }



}
