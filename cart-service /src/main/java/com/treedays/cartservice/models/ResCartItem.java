package com.treedays.cartservice.models;

import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.UUID;

public class ResCartItem {

    private UUID id;
    private Product product;

    private int qty;
    private BigDecimal subTotal;

    public ResCartItem(UUID id, Product product, int qty) {
        this.id = id;
        this.product = product;
        this.qty = qty;
        this.subTotal = product.getPrice().multiply(new BigDecimal(qty));
    }

    public BigDecimal getSubTotal() {
        return product.getPrice().multiply(new BigDecimal(qty));
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }
}
