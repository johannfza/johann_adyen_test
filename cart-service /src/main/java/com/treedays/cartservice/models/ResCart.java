package com.treedays.cartservice.models;

import com.treedays.cartservice.models.db.Cart;

import java.math.BigDecimal;
import java.util.List;

public class ResCart {

    private String id;
    private String userId;
    private BigDecimal total;

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    private List<ResCartItem> items;

    public ResCart() {
    }

    public ResCart(String id, String userId, List<ResCartItem> items) {
        this.id = id;
        this.userId = userId;
        this.items = items;
        this.total = items.stream().map(item -> item.getSubTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<ResCartItem> getItems() {
        return items;
    }

    public void setItems(List<ResCartItem> items) {
        this.items = items;
    }

    public BigDecimal getTotal() {
        BigDecimal total = items.stream().map(item -> item.getSubTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public int getMinorTotal(){
        BigDecimal hundred = BigDecimal.valueOf(100);
        return Integer.valueOf(this.total.multiply(hundred).intValue());
    }


}
