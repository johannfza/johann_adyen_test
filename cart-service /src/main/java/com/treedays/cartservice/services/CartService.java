package com.treedays.cartservice.services;

import com.treedays.cartservice.models.db.Cart;
import com.treedays.cartservice.models.db.CartItem;
import com.treedays.cartservice.repositories.CartItemRepository;
import com.treedays.cartservice.repositories.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CartService {

    private CartRepository cartRepository;
    private CartItemRepository cartItemRepository;

    @Autowired
    public CartService(CartRepository cartRepository, CartItemRepository cartItemRepository){
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
    }

    public List<Cart> getAll() {
        return cartRepository.findAll();
    }

    public Optional<Cart> findById(String id){
        return cartRepository.findById(id);
    }

    public Cart findByCustomerId(String id) {
        return cartRepository.findByUserId(id);
    }

    public Cart save(Cart cart) {
        return cartRepository.saveAndFlush(cart);
    }

    public Cart clearCart(String id) {
        Optional<Cart> optCart = cartRepository.findById(id);
        if (!optCart.isPresent()) {
            return optCart.orElse(null);
        }
        cartItemRepository.deleteCartItemsByCartId(id);
        Cart updatedCart = cartRepository.findById(id).orElse(null);
        return updatedCart;

    }

    public Cart addItemByCartId(CartItem cartItem, String cart_id) {
        // Find Shopping Cart else return [null]
        Optional<Cart> optCart = cartRepository.findById(cart_id);
        if (!optCart.isPresent()) {
            return optCart.orElse(null);
        }
//        // Check for product id else return [null]
//        Optional<Product> optProduct = productRepository.findById(cartItem.getProduct().getId());
//        if (!optProduct.isPresent()) {
//            System.out.println("No such product");
//            return null;
//        }
        // Set shopping cart and product variable
        Cart cart = optCart.get();
//        Product product = optProduct.get();
        // Persist data
        return persistItem(cart, cartItem);
    }

    public Cart persistItem(Cart cart, CartItem cartItem) {
        Cart updatedCart = new Cart();
        // Check if found cart contains product.
        if (!cart.containsProductId(cartItem.getProductId())) {
            // If product not found add new item
            System.out.println("Cart does not contain product");
            CartItem tempCartItem = new CartItem(cartItem.getProductId(), cartItem.getQty(), cart);
            System.out.println("Adding new product to cart...");
            cartItemRepository.saveAndFlush(tempCartItem);
            cart.getItems().add(tempCartItem);
            updatedCart = cart;
        } else {
            // If product already in list increment qty
            System.out.println("Current cart contains product...");
            for (int i = 0; i < cart.getItems().size(); i++) {
                if (cart.getItems().get(i).getProductId().equals(cartItem.getProductId())) {
                    System.out.println("Incrementing...");
                    cart.getItems().get(i).setQty(cart.getItems().get(i).getQty() + cartItem.getQty());
                    cartRepository.saveAndFlush(cart);
                    // Save update shopping cart
                    updatedCart = cartRepository.findById(cart.getId()).get();
                }
            }
        }
        return updatedCart;
    }

    public CartItem removeCartItem(UUID id) {
        Optional<CartItem> optionalCartItem = cartItemRepository.findById(id);
        if (!optionalCartItem.isPresent()) {
            return optionalCartItem.orElse(null);
        }
        cartItemRepository.deleteById(id);
        return optionalCartItem.get();
    }

}
