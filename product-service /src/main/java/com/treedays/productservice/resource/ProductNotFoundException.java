package com.treedays.productservice.resource;

class ProductNotFoundException extends RuntimeException {

    ProductNotFoundException(String id) {
        super("Could not find product with id: " + id);
    }

}