package com.treedays.cartservice.models;

public class ResAmount {

    private int value;
    private String currency;

    // Default currency = "SDG"
    public ResAmount(int value) {
        this.value = value;
        this.currency = "SGD";
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
